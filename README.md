This is a template for a blog using Gitlab pages, hexojs and NetlifyCMS

## Fork the repo

## Clone repo
    git clone ...

## Install hexo
    npm install hexo -g

## Install dependencies
    npm install

## Test it local
    hexo serve
